var classeEnum = {A:1, B:2, C:3, D:4, E:5};

// Classe Adresse
function Adresse(ip1, ip2, ip3, ip4){
	this.ip1 = ip1;
	this.ip2 = ip2;
	this.ip3 = ip3;
	this.ip4 = ip4;
}

// Classe Adresse Hote
function AdresseHote(classe, adresse, adresseBroad, adresseReseau){
	this.classe = classe;
	this.adresse = adresse;
	this.adresseBroad = adresseBroad;
	this.adresseReseau = adresseReseau;
}

AdresseHote.prototype.adresseBroadOf = function() {
	this.adresseBroad = new Adresse();
	switch (this.classe) {
		case 1:
			this.adresseBroad.ip1 = this.adresse.ip1;
			this.adresseBroad.ip2 = '255';
			this.adresseBroad.ip3 = '255';
			this.adresseBroad.ip4 = '255';
			break;
		case 2:
			this.adresseBroad.ip1 = this.adresse.ip1;
			this.adresseBroad.ip2 = this.adresse.ip2;
			this.adresseBroad.ip3 = '255';
			this.adresseBroad.ip4 = '255';
			break;
		case 3:
			this.adresseBroad.ip1 = this.adresse.ip1;
			this.adresseBroad.ip2 = this.adresse.ip2
			this.adresseBroad.ip3 = this.adresse.ip3
			this.adresseBroad.ip4 = '255';
			break;
		default:
			alert('Pas de reponse pour l\'instant');
			break;
	}
	return this.adresseBroad;
};

AdresseHote.prototype.adresseReseauOf = function() {
	this.adresseReseau = new Adresse();
	switch (this.classe) {
		case 1:
			this.adresseReseau.ip1 = this.adresse.ip1;
			this.adresseReseau.ip2 = '0';
			this.adresseReseau.ip3 = '0';
			this.adresseReseau.ip4 = '0';
			break;
		case 2:
			this.adresseReseau.ip1 = this.adresse.ip1;
			this.adresseReseau.ip2 = this.adresse.ip2;
			this.adresseReseau.ip3 = '0';
			this.adresseReseau.ip4 = '0';
			break;
		case 3:
			this.adresseReseau.ip1 = this.adresse.ip1;
			this.adresseReseau.ip2 = this.adresse.ip2
			this.adresseReseau.ip3 = this.adresse.ip3
			this.adresseReseau.ip4 = '0';
			break;
		default:
			alert('Pas de reponse pour l\'instant');
			break;
	}
	return this.adresseReseau;
};

AdresseHote.prototype.classeOf = function() {
	if (parseInt(this.adresse.ip1)<127 && parseInt(this.adresse.ip1)>0) { // 1.0.0.0 --> 126.255.255.255
		this.classe = classeEnum.A;
	}else if (parseInt(this.adresse.ip1)<192 && parseInt(this.adresse.ip1)>127) { // 128.0.0.0 --> 191.255.255.255
		this.classe = classeEnum.B;
	}else if (parseInt(this.adresse.ip1)<224 && parseInt(this.adresse.ip1)>191) { // 192.0.0.0 --> 223.255.255.255
		this.classe = classeEnum.C;
	}else if (parseInt(this.adresse.ip1)<240 && parseInt(this.adresse.ip1)>223) { // 224.0.0.0 --> 239.255.255.255
		this.classe = classeEnum.D;
	}else if (parseInt(this.adresse.ip1)<256 && parseInt(this.adresse.ip1)>239) { // 240.0.0.0 --> 255.255.255.255
		this.classe = classeEnum.E;
	}
	return this.classe;
};


function suivant(enCours, suivant, limite){ 
	if (enCours.value.length == limite){
		document.ip[suivant].focus();
	}
}


function validerIP(ip1, ip2, ip3, ip4){
	var ip = document.ip[ip1].value + '.' + document.ip[ip2].value + '.' + document.ip[ip3].value + '.' + document.ip[ip4].value;
	if (validator.isIP(ip)){
		var adr = new Adresse();		
		adr.ip1 = document.ip[ip1].value;
		adr.ip2 = document.ip[ip2].value;
		adr.ip3 = document.ip[ip3].value;
		adr.ip4 = document.ip[ip4].value;

		var adrH = new AdresseHote();
		adrH.adresse = adr;
		adrH.classeOf();
		// adrH.adresseBroad = adresseBroadOf(adrH);
		adrH.adresseBroadOf();
		adrH.adresseReseauOf();
		console.log('Adresse Hote:\n');
		console.log(adrH.adresse);
		
		console.log('Classe:');
		console.log(adrH.classe);

		console.log('Adresse BroadCast:\n');
		console.log(adrH.adresseBroad);

		console.log('Adresse Reseau:\n');
		console.log(adrH.adresseReseau);
	}else{
		alert("Not good");
	}
}
